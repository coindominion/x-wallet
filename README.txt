The code in this repository is used on the commercial X-Wallet drive systems sold by Coin Dominion LLC (http://www.coindominion.com).


Coin Dominion LLC accepts no liability regarding the use of the code in this repository. Use of purchased X-Wallet systems are covered by Coin Dominion LLC's EULA.

***USE CODE IN THIS REPOSITORY AT YOUR OWN RISK***

About:
The X-Wallet project allows the use of cryptocurrency clients with more security and flexibility. 

Normally, a client such as Bitcoin-qt writes the currency's blockchain and additional data to a hidden folder on the host computer. This is not ideal for many reasons:
  - All data exists on one host computer, leaving itself vulnerable to system failure
  - If the host computer is not mobile, then neither is the client
  - Accessing hidden system drives is less convenient
  - If the host computer is connected to the internet, malicious code can search for the data directory and attack it

Utilizing an X-Wallet system allows the client to store data in a specified folder structure. This has many benefits:
  - Folder location is customizable
  - If implemented on an external drive, the client, blockchain, and wallet can all be removed from the host computer, creating an accessible cold-storage solution we call a freezer.
  - Implementing on an external hard drive can allow clients cross-platform (Mac / PC) access to blockchain and wallet data
  - Blockchains are easily located in case of file corruption and iterative backups are easy to make
 

With an X-Wallet implementation, a user can organize, protect, and travel with their currencies. 
Implementing on an external drive allows portable access.* 
Only one blockchain and wallet.dat file is required per cryptocurrency; the operating systems use the same set of blockchain data.


*System memory and logs will show the use of the installed clients; the system is portable in the sense that you can travel with it and launch it on different systems.