ECHO OFF
title Coin Dominion's Portable Client Launcher
CLS
ECHO This will launch the Bitcoin client.
ECHO.
ECHO By using this software you are agreeing electronically to the
ECHO terms of this software's End User License Agreement 
ECHO (http://coindominion.com/legal/x-wallet-eula/). If you do
ECHO not agree to these terms, do not use the software.
ECHO.
ECHO Visit CoinDominion.com for the latest information.
ECHO.
PAUSE
START %~dp0Clients\Bitcoin-32bit\bitcoin-qt.exe -datadir=../data/BTC
CLS
EXIT