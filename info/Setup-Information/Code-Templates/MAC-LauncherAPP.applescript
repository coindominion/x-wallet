tell application "Finder"
	set ppath to POSIX path of ((container of (path to me)) as alias)
	set appLaunch to ppath & "Clients/EXAMPLE-Qt.app"
	display dialog "This will launch the EXAMPLE client." & return & return & "By using this software you are agreeing electronically to the terms of this software's End User License Agreement" & return & "(http://www.coindominion.com/x-wallet-eula/). If you do not agree to these terms, do not use the software." & return & return & "Visit CoinDominion.com for the latest information." & return & return & "Click OK to continue." & return with title "Coin Dominion's Portable Client Launcher"
end tell
try
	tell application appLaunch
		with timeout of 360 seconds
			activate
		end timeout
	end tell
end try